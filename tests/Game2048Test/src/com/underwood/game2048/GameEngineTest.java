package com.underwood.game2048;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.underwood.game2048.GameEngine;
import com.underwood.game2048.GameEngine.Dificulty;

public class GameEngineTest extends TestCase {
	
	public void testCreateGameEngine(){
		for (Dificulty dif : Dificulty.values()) {
			GameEngine engine = new GameEngine(dif);
			Assert.assertEquals("board sizes don't match for "+dif, dif.getBoardSize(), engine.getBoardSize());
			Assert.assertEquals("board is not empty for "+dif, 0, countNonEmptyCells(engine));
			Assert.assertFalse("win flag is set for "+dif, engine.isUserWon());
			Assert.assertFalse("loose flag is set for "+dif, engine.isUserLost());
		}
	}
	
	public void testCreateCell(){
		for (Dificulty dif : Dificulty.values()) {
			GameEngine engine = new GameEngine(dif);
			int lastCount = countNonEmptyCells(engine);
			for (int i = 0; i < 5; i++) {
				engine.createNewCells();
				int count = countNonEmptyCells(engine);
				Assert.assertEquals("Filled cell count doesn't match for "+dif, lastCount+dif.getTilesToCreate(), count);
				lastCount = count;
			}
		}
	}
	
	public void test1cellMove(){
		for (Dificulty dif : Dificulty.values()) {
			GameEngine engine = new GameEngine(dif);
			int lastCount = countNonEmptyCells(engine);
			for (int i = 0; i < 5; i++) {
				engine.createNewCells();
				int count = countNonEmptyCells(engine);
				Assert.assertEquals("Filled cell count doesn't match ", lastCount+dif.getTilesToCreate(), count);
				lastCount = count;
			}
		}
	}
	
	private static int countNonEmptyCells(GameEngine engine) {
		int count = 0;
		for (int row = 0; row < engine.getBoardSize(); row++) {
			for (int column = 0; column < engine.getBoardSize(); column++) {
				if (engine.getCellValue(row, column) != 0) {
					count++;
				}
			}	
		}
		return count;
	}
}
