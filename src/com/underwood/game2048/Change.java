package com.underwood.game2048;

/**
 * Class that contains info of single event happened on game board 
 * (cell created, cell moved or cells merged)
 */
public class Change {
	private final Cell cell;
	private final Cell otherCell;

	public Change(Cell cell) {
		this(cell, null);
	}
	
	public Change(Cell cell, Cell otherCell) {
		super();
		this.cell = cell;
		this.otherCell = otherCell;
	}
	
	/**
	 * @return primary cell (created, moved to or merged into cell)
	 */
	public Cell getCell() {
		return cell;
	}

	/**
	 * @return secondary cell (moved from or merged from cell)
	 */
	public Cell getOtherCell() {
		return otherCell;
	}
	
	/**
	 * @return true if change corresponds to cell created event
	 */
	public boolean isNewCreatedEvent(){
		return otherCell == null;
	}
	
	/**
	 * @return true if change corresponds to merge of 2 cells
	 */
	public boolean isMergedEvent(){
		return otherCell != null && cell.getValue() != 0;
	}
}
