package com.underwood.game2048;

/**
 * Holds information about cell position and value
 */
public class Cell {
	private final int row;
	private final int column;
	private final int value;
	
	public Cell (int row, int column, int value) {
		this.row = row;
		this.column = column;
		this.value = value;
	}
	
	/**
	 * @return cell's row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return cell's column
	 */
	public int getColumn() {
		return column;
	}
	
	/**
	 * @return cell's value
	 */
	public int getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return row+":"+column+"("+value+")";
	}
}
