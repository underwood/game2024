package com.underwood.game2048;

import java.util.List;

import com.underwood.game2048.GameEngine.Dificulty;
import com.underwood.game2048.GameEngine.GameListener;
import com.underwood.game2048.GameEngine.UserMove;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class MainActivity extends Activity {
	private GameFieldView fieldView;
	private GameEngine engine;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Dificulty dificulty = GameSettings.getInstance(this).getDificultyLevel();
		engine = new GameEngine(dificulty);
		engine.setListener(gameListener);

		ViewGroup group = (ViewGroup) findViewById(R.id.gameViewContainer);
		fieldView = new GameFieldView(this);
		group.addView(fieldView);
		fieldView.setGameEngine(engine);
		
		findViewById(R.id.left).setOnClickListener(onClickListener);
		findViewById(R.id.up).setOnClickListener(onClickListener);
		findViewById(R.id.right).setOnClickListener(onClickListener);
		findViewById(R.id.down).setOnClickListener(onClickListener);

		engine.createNewCells();
	}
	
	OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			boolean moved = false;
			switch (v.getId()) {
			case R.id.left:
				moved = engine.handleUserMove(UserMove.LEFT);
				break;
			case R.id.up:
				moved = engine.handleUserMove(UserMove.UP);
				break;
			case R.id.right:
				moved = engine.handleUserMove(UserMove.RIGHT);
				break;
			case R.id.down:
				moved = engine.handleUserMove(UserMove.DOWN);
				break;
			}
			if (moved && !engine.isUserWon() && !engine.isUserLost()) {
				engine.createNewCells();
			}
		}
	};
	
	GameListener gameListener = new GameListener() {
		
		@Override
		public void onUserWon() {
			showRestartDialog(R.string.dlg_won_title, R.string.dlg_won_message, false);
		}
		
		@Override
		public void onUserLost() {
			showRestartDialog(R.string.dlg_lost_message, R.string.dlg_lost_message, false);
		}
		
		@Override
		public void onBoardUpdate(List<Change> changeList) {
			fieldView.animateBoardChanges(changeList);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_restart:
			restart(engine.isUserLost());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void showDifficultyDialog(){
		new AlertDialog.Builder(this)
			.setTitle(R.string.dlg_dificulty_title)
			.setItems(R.array.dificulty_levels, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Dificulty dificulty;
					switch (which) {
					case 0:
						dificulty = Dificulty.LOW;
						break;
					case 2:
						dificulty = Dificulty.HIGH;
						break;
					default:
						dificulty = Dificulty.MEDIUM;
						break;
					}
					GameSettings.getInstance(MainActivity.this).setDificultyLevel(dificulty);
					restart(true);
				}})
			.show();
	}

	private void showRestartDialog(int titleTextid, int messageTextId, boolean isSticky){
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setTitle(titleTextid)
			.setMessage(messageTextId)
			.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					showDifficultyDialog();
				}});
		if (isSticky){
			builder.setCancelable(false);
		} else {
			builder.setNegativeButton(R.string.btn_cancel, null);
		}
		builder.create().show();
	}

	private void restart(boolean force) {
		if (!force) {
			showRestartDialog(R.string.dlg_restart_title, R.string.dlg_restart_message, false);
		} else {
			Dificulty dificulty = GameSettings.getInstance(this).getDificultyLevel();
			engine = new GameEngine(dificulty);
			engine.setListener(gameListener);
			fieldView.setGameEngine(engine);
			engine.createNewCells();
		}
	}
	
}
