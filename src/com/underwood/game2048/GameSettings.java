package com.underwood.game2048;

import com.underwood.game2048.GameEngine.Dificulty;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class GameSettings {
	private static final String KEY_DIFICULTY = "dificulty";
	private SharedPreferences preferences;
	
	private GameSettings(Context context) {
		preferences = PreferenceManager
				.getDefaultSharedPreferences(context.getApplicationContext());
	}
	
	public static GameSettings getInstance(Context context) {
		return new GameSettings(context);
	}
	
	public Dificulty getDificultyLevel(){
		return Dificulty.values()[
		    preferences.getInt(KEY_DIFICULTY, Dificulty.MEDIUM.ordinal())];
	}
	
	public void setDificultyLevel(Dificulty dificulty){
		preferences.edit()
			.putInt(KEY_DIFICULTY, dificulty.ordinal())
			.commit(); 
	}
}
