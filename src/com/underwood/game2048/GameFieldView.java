package com.underwood.game2048;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

public class GameFieldView extends FrameLayout {
	private static final String TAG = "GameFieldView";
	private static final boolean TRACE = true;
	private View[][] cells;
	private GameEngine gameEngine;
	
	private final float cellMargins;
	private final CellRendererHelper rendererHelper; 
	private final ChangeAnimator changeAnimator;
	
	public GameFieldView(Context context) {
		this(context, null);
	}

	public GameFieldView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public GameFieldView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setBackgroundResource(R.drawable.board_bcg);
		cellMargins = getResources().getDimension(R.dimen.board_cell_margins);
		rendererHelper = new CellRendererHelper();
		changeAnimator = new ChangeAnimator();
	}
	
	public void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
		this.removeAllViews();
		final int boardSize = gameEngine.getBoardSize();
		cells = new View[boardSize][];
		for (int row = 0; row < boardSize; row++) {
			cells[row] = new View[boardSize];
			for (int column = 0; column < boardSize; column++) {
				cells[row][column] = LayoutInflater.from(getContext()).inflate(R.layout.board_cell, this, false);
				addView(cells[row][column]);
				rendererHelper.updateCellView(cells[row][column], gameEngine.getCellValue(row, column));
			}
		}
	}
	
	public void animateBoardChanges(List<Change> changes) {
		changeAnimator.handleChanges(changes);
	}
	
	private int getCellOffset(int index, int parentSize) {
		if (gameEngine.getBoardSize() > 0) {
			return (int)(getCellSize(parentSize) * index + cellMargins * (index + 1));
		}
		return 0;
	}
	
	private float getCellSize(int parentSize) {
		final int boardSize = gameEngine.getBoardSize();
		if (boardSize > 0) {
			return (int)((parentSize - (boardSize+1)*cellMargins) / boardSize);
		}
		return 0;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int size = Math.min(getSize(widthMeasureSpec), getSize(heightMeasureSpec));
		if (size != Integer.MAX_VALUE) {
			// make view square
	        setMeasuredDimension(size, size);
	        final int boardSize = gameEngine.getBoardSize();
			final int measureSpec = MeasureSpec.makeMeasureSpec((int)getCellSize(getWidth()), MeasureSpec.EXACTLY);
			for (int row = 0; row < boardSize; row++) {
				for (int column = 0; column < boardSize; column++) {
					measureChild(cells[row][column], measureSpec, measureSpec);
				}
			}
		} else {
			// can't determine size to make this view square  
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}
	private static int getSize(int measureSpec){
		return MeasureSpec.getMode(measureSpec) != MeasureSpec.UNSPECIFIED 
			? MeasureSpec.getSize(measureSpec) : Integer.MAX_VALUE;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		final int cellSize = (int)getCellSize(getWidth());
		final int boardSize = gameEngine.getBoardSize();
		Drawable cellBcgDrawable = rendererHelper.getDrawableForValue(0);
		for (int row = 0; row < boardSize; row++) { // TODO measure performance. create cache?
			for (int column = 0; column < boardSize; column++) {
				int x = getCellOffset(column, getWidth());
				int y = getCellOffset(row, getHeight());
				cellBcgDrawable.setBounds(x, y, x + cellSize, y + cellSize);
				cellBcgDrawable.draw(canvas);
			}
		}
	}

	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		final int cellSize = (int)getCellSize(getWidth());
		final int boardSize = gameEngine.getBoardSize();
		for (int row = 0; row < boardSize; row++) {
			for (int column = 0; column < boardSize; column++) {
				int x = getCellOffset(column, getWidth());
				int y = getCellOffset(row, getHeight());
				cells[row][column].layout(x, y, x + cellSize, y + cellSize);
			}
		}
	}
	
	View getCellView(Cell cell){
		return cells[cell.getRow()][cell.getColumn()];
	}
	
	void syncCellViewState(Cell cell) {
		if (TRACE) Log.d(TAG, "syncCellViewState "+cell+" "+gameEngine.getCellValue(cell.getRow(), cell.getColumn()));
		rendererHelper.updateCellView(getCellView(cell), gameEngine.getCellValue(cell.getRow(), cell.getColumn()));
	}
	
	private class CellRendererHelper {
		private final SparseArray<Drawable> valueToDrawable = new SparseArray<Drawable>();
				
		public CellRendererHelper() {
			valueToDrawable.append(0, newDrawable(R.color.board_cell_bcg_color));
			valueToDrawable.append(2, newDrawable(R.color.board_2_bcg_color));
			valueToDrawable.append(4, newDrawable(R.color.board_4_bcg_color));
			valueToDrawable.append(8, newDrawable(R.color.board_8_bcg_color));
			valueToDrawable.append(16, newDrawable(R.color.board_16_bcg_color));
			valueToDrawable.append(32, newDrawable(R.color.board_32_bcg_color));
			valueToDrawable.append(64, newDrawable(R.color.board_64_bcg_color));
			valueToDrawable.append(128, newDrawable(R.color.board_128_bcg_color));
			valueToDrawable.append(256, newDrawable(R.color.board_256_bcg_color));
			valueToDrawable.append(512, newDrawable(R.color.board_512_bcg_color)); 
			valueToDrawable.append(1024, newDrawable(R.color.board_1024_bcg_color));
			valueToDrawable.append(2048, newDrawable(R.color.board_2048_bcg_color));
		}
		
		private Drawable newDrawable(int colorId) {
			GradientDrawable drawable = new GradientDrawable();
			drawable.setColor(getResources().getColor(colorId));
			drawable.setCornerRadius(getResources().getDimension(R.dimen.board_cell_bcg_radius));
			return drawable;
//			GradientDrawable shapeDrawable = (GradientDrawable)getResources().getDrawable(R.drawable.board_cell_bcg);
//			shapeDrawable.setColor(getResources().getColor(colorId));
//			return shapeDrawable;
		}
		/**
		 * @param value
		 * @return {@link Drawable} that corresponds to a value
		 */
		public Drawable getDrawableForValue(int value) {
			return valueToDrawable.get(value, valueToDrawable.get(0));
		}
		
		/**
		 * Updates text, style and visibility of a view to correspond to a cell value
		 * @param cellView
		 * @param cellValue
		 */
		@SuppressWarnings("deprecation")
		public void updateCellView(View cellView, int cellValue) {
			if (cellValue > 0) {
				cellView.setVisibility(View.VISIBLE);
				cellView.findViewById(R.id.root).setBackgroundDrawable(getDrawableForValue(cellValue));
				((TextView)cellView.findViewById(R.id.text)).setText(Integer.toString(cellValue));
			} else {
				cellView.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Handler to perform actions after moves of cells. (Static to avoid leaks)
	 */
	static class CellSettleHandler extends Handler {
		public static final int MSG_CHANGE = 1;
		
		private WeakReference<GameFieldView> boardViewRef;
		
		public CellSettleHandler(GameFieldView gameBoardView) {
			boardViewRef = new WeakReference<GameFieldView>(gameBoardView);
		}
		
		public void handleMessage(android.os.Message msg) {
			if (msg.what == MSG_CHANGE){
				GameFieldView gameBoardView = boardViewRef.get();
				if (gameBoardView == null) return;
				Set<Cell> update = new HashSet<Cell>();
				@SuppressWarnings("unchecked")
				List<Change> obj = (List<Change>) msg.obj;
				for (Change change : obj) {
					update.add(change.getCell());
					if (change.getOtherCell() != null)
						update.add(change.getOtherCell());
				}
				for (Cell cell : update) {
					gameBoardView.syncCellViewState(cell);
				}
				
//				for (Change change : (List<Change>) msg.obj) {
//					syncCellViewState(change.getCell());
//					if (change.getOtherCell() != null)
//						syncCellViewState(change.getOtherCell());
//				}
				for (Change change : obj) {
					final Cell cell = change.getCell();
					if (change.getOtherCell() == null || cell.getValue() != 0) {
						// cell was created
						// or cells were merged
						animateScale(cell);
					}
				}
			}
		}
		private void animateScale(Cell cell) {
			GameFieldView gameBoardView = boardViewRef.get();
			if (gameBoardView != null) {
				Animation anim = AnimationUtils.loadAnimation(gameBoardView.getContext(), R.anim.scale_anim);
				gameBoardView.getCellView(cell).startAnimation(anim);
			}
		}		
	};
	private class ChangeAnimator implements AnimationListener {
		private CellSettleHandler handler;
		private boolean animationEndHandled;
		
		public ChangeAnimator() {
			 handler = new CellSettleHandler(GameFieldView.this);
		}
		
		public void handleChanges(List<Change> changes) {
			// stop all animations
			for (Change change : changes) {
				getCellView(change.getCell()).setAnimation(null);				
				if (change.getOtherCell() != null) {
					getCellView(change.getOtherCell()).setAnimation(null);
				}
			}

			ArrayList<Change> changesAfterMove = new ArrayList<Change>(changes);
			boolean hasMoveAnimations = false;
			
			for (Change change : changes) {
				if (change.getOtherCell() != null) {
					hasMoveAnimations = true;
					View otherCellView = getCellView(change.getOtherCell());
					Animation animation = createTranslateAnimation (getCellView(change.getCell()), otherCellView);
					animation.setAnimationListener(this);
					otherCellView.startAnimation(animation);
				}
			}
			Message msg = handler.obtainMessage(CellSettleHandler.MSG_CHANGE, 0, 0, changesAfterMove);
			handler.sendMessageDelayed(msg, hasMoveAnimations ? getResources().getInteger(R.integer.anim_duration)-10: 0);
		}
		
		private Animation createTranslateAnimation (View to, View from) {
			TranslateAnimation animation = new TranslateAnimation(0, to.getLeft() - from.getLeft(), 0, to.getTop() - from.getTop());
			animation.setDuration(getResources().getInteger(R.integer.anim_duration));
			return animation;
		}
		
		@Override
		public void onAnimationStart(Animation animation) {
			animationEndHandled = false;
		}
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		@Override
		public void onAnimationEnd(Animation animation) {
			if (!animationEndHandled){
				animationEndHandled = true;
			}
		}
	}
}
