package com.underwood.game2048;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.util.Log;

/**
 * Class contains logic of the game, performs calculations and notifies of changes
 */
public class GameEngine {
	private static final String TAG = "GameEngine";
	private static final boolean TRACE = true;
	
	/**
	 * Game difficulty
	 */
	public enum Dificulty {
		LOW(5, 1), MEDIUM(4, 1), HIGH(4, 2);
		
		private final int tilesToCreate;
		private final int boardSize;
		
		private Dificulty(int boardSize, int tilesToCreate) {
			this.tilesToCreate = tilesToCreate;
			this.boardSize = boardSize;
		}
		/**
		 * @return board size
		 */
		int getBoardSize() {
			return boardSize;
		}
		/**
		 * @return tiles to create after user move
		 */
		int getTilesToCreate() {
			return tilesToCreate;
		}
	}
	
	static interface GameListener {
		void onBoardUpdate(List<Change> changeList);
		void onUserWon();
		void onUserLost();
	}
	
	/**
	 * User moves
	 */
	public enum UserMove {
		UP, DOWN, LEFT, RIGHT;
	}
	
	private static final int MIN_CELL_VALUE = 2;
	private static final int MAX_CELL_VALUE = 2048;
	
	private final Dificulty dificulty;
	private final int[] board;
	private final Random randomEngine;
	private GameListener listener;
	private final List<Change> changeList;
	private boolean userWon;
	private boolean userLost;
	
	public GameEngine(Dificulty dificulty) {
		this.dificulty = dificulty;
		board = new int[dificulty.getBoardSize()*dificulty.getBoardSize()];
		randomEngine = new Random();
		changeList = new ArrayList<Change>();
	}
	
	public void setListener(GameListener listener) {
		this.listener = listener;
	}

	/**
	 * creates new cells at start and after user move
	 */
	public void createNewCells() {
		// list all empty cell
		ArrayList<Integer> emptyCells = new ArrayList<Integer>();
		for (int i = 0; i < board.length; i++) {
			if (board[i] == 0) {
				emptyCells.add(i);
			}
		}
		
		for (int i = 0; i < dificulty.getTilesToCreate(); i++) {
			// get random index to create cell
			int index = emptyCells.remove(randomEngine.nextInt(emptyCells.size()));
			board[index] = MIN_CELL_VALUE;
			onCellCreated(new Cell(index / dificulty.getBoardSize(), index % dificulty.getBoardSize(), board[index]));
			if (emptyCells.isEmpty()) break;
		}
		checkLooseCondition();
		displayChanges();
	}
	
	private void checkLooseCondition() {
		userLost = false;
		boolean canMove = false;
		for (int i = 0; i < board.length; i++) {
			if (board[i] == 0) {
				canMove = true;
				break; 
			}
		}
		if (!canMove) {
			outer : for (int firstDim = 0; firstDim < dificulty.boardSize; firstDim++) {
				for (int secondDim = 0; secondDim < dificulty.boardSize-1; secondDim++) {
					if (getCellValue(firstDim, secondDim) == getCellValue(firstDim, secondDim+1) 
					 || getCellValue(secondDim, firstDim) == getCellValue(secondDim+1, firstDim)) {
						canMove = true;
						break outer; 
					}
				}	
			}
		}
		userLost = !canMove;
		if (userLost) {
			if (listener != null) listener.onUserLost();
		}
	}

	/**
	 * @param row row of the cell
	 * @param column column of the cell
	 * @return value of the cell
	 */
	public int getCellValue(int row, int column) {
		return board[row*dificulty.getBoardSize() + column];
	}
	
	private void setCellValue(int row, int column, int value) {
		board[row*dificulty.getBoardSize() + column] = value;
	}
	
	/**
	 * Handles user move
	 * @return whether user action results moves on board
	 */
	public boolean handleUserMove(UserMove move) {
		if (TRACE) Log.d(TAG, "== board before move ==");
		if (TRACE) Log.d(TAG, debugPrint());
		boolean cellsMoved = false;
		BoardIterator iterator = new BoardIterator(move, dificulty.getBoardSize());
		while(iterator.hasNext()) {
			BoardSubIterator subIterator = iterator.next();
			BoardSubIterator occupiedCellRef = null;
			
			boolean canMerge = true;
			while(subIterator.moveToNext()) {
				final int cellValue = getCellValue(subIterator.getRow(), subIterator.getColumn());
				if (cellValue != 0) {
					//cell is not empty
					if (occupiedCellRef != null && cellValue == getCellValue(occupiedCellRef.getRow(), occupiedCellRef.getColumn()) && canMerge) {
						// merge
						move(occupiedCellRef.getRow(), occupiedCellRef.getColumn(), subIterator.getRow(), subIterator.getColumn());
						cellsMoved = true;
						canMerge = false;
					} else {
						if (occupiedCellRef == null) {
							occupiedCellRef = subIterator.newReseted();
						}
						occupiedCellRef.moveToNext();
						if (!occupiedCellRef.equals(subIterator)) {
							move(occupiedCellRef.getRow(), occupiedCellRef.getColumn(), subIterator.getRow(), subIterator.getColumn());
							cellsMoved = true;
						}
						canMerge = true;
					}
				}
			}				
		}
		
		if (userWon) {
			if (listener != null) listener.onUserWon();
			displayChanges();
		}
		if (TRACE) Log.d(TAG, "== board after move ==");
		if (TRACE) Log.d(TAG, debugPrint());
		
		return cellsMoved;
	}
	
	private static class BoardSubIterator {
		private final UserMove direction;
		private final int boardSize;
		private final int primaryIndex;
		private int subindex;
		private int delta;
		
		public BoardSubIterator(int primaryIndex, UserMove direction, int boardSize) {
			this.direction = direction;
			this.primaryIndex = primaryIndex;
			this.boardSize = boardSize;
			if (direction == UserMove.LEFT || direction == UserMove.UP){
				delta = 1;
				subindex = -1;
			} else {
				delta = -1;
				subindex = boardSize;
			}
		}
		
		/**
		 * Moves 1 step traversing board 
		 * @return true if step succeeds
		 */
		public boolean moveToNext() {
			subindex += delta;
			return 0 <= subindex && subindex < boardSize;
		}
		
		/**
		 * @return current row
		 */
		public int getRow() {
			return direction == UserMove.LEFT || direction == UserMove.RIGHT ? primaryIndex : subindex;
		}
		/**
		 * @return current column
		 */
		public int getColumn() {
			return direction == UserMove.LEFT || direction == UserMove.RIGHT ? subindex : primaryIndex;
		}
		
		public BoardSubIterator newReseted() {
			return new BoardSubIterator(primaryIndex, direction, boardSize);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj instanceof BoardSubIterator) {
				final BoardSubIterator other = (BoardSubIterator) obj;
				return primaryIndex	== other.primaryIndex && subindex == other.subindex && direction == other.direction;
			}
			return false;
		}
	}
	
	// Traversal rule
	// case LEFT: // row: top to bottom; column: left to right
	// case UP: // column: left to right; row: top to bottom
	// case RIGHT: // row: top to bottom; column: right to left
	// case DOWN: // column: left to right; row: bottom to top  
	private static class BoardIterator{
		private final UserMove direction;
		private final int boardSize;
		private int primaryIndex;
		
		public BoardIterator(UserMove direction, int boardSize) {
			this.direction = direction;
			this.boardSize = boardSize;
			primaryIndex = -1;
		}
		
		/**
		 * @return DimensionSubIterator to traverse board in 2nd dimension 
		 */
		public BoardSubIterator next() {
			primaryIndex++;
			if (primaryIndex < boardSize){
				return new BoardSubIterator(primaryIndex, direction, boardSize);
			};
			return null;
		}
		/**
		 * @return true if call to next will succeed
		 */
		public boolean hasNext(){
			return primaryIndex < boardSize - 1;
		}
	}
	
	/**
	 * Moves a cell [rowFrom:columnFrom] => [rowTo:columnTo], optionally merges
	 */
	private void move(int rowTo, int columnTo, int rowFrom, int columnFrom){
		int toValue = getCellValue(rowTo, columnTo);
		int fromValue = getCellValue(rowFrom, columnFrom);
		onMove(new Cell(rowTo, columnTo, toValue), new Cell(rowFrom, columnFrom, fromValue));
		setCellValue(rowTo, columnTo, toValue+fromValue);
		setCellValue(rowFrom, columnFrom, 0);
		if (MAX_CELL_VALUE <= toValue+fromValue) {
			userWon = true;
		}
	}
	
	private void onMove(Cell to, Cell from) {
		System.out.println("move "+to+"<="+from);
		changeList.add(new Change(to, from));
	}
	
	private void onCellCreated(Cell cell) {
		changeList.add(new Change(cell));
	}
	
	private void displayChanges() {
		if (listener != null) listener.onBoardUpdate(changeList);
		changeList.clear();
	}
	
	/**
	 * @return board size
	 */
	public int getBoardSize() {
		return dificulty.getBoardSize();
	}
	
	public boolean isUserWon() {
		return userWon;
	}
	
	public boolean isUserLost() {
		return userLost;
	}
	
	
	void loadFromArray(int[] board) {
		if (this.board.length != board.length) {
			throw new IllegalArgumentException("Board size must be "+board.length);
		}
		System.arraycopy(board, 0, this.board, 0, board.length);
	}
	
	int[] saveToArray(){
		int[] saved = new int[board.length];
		System.arraycopy(board, 0, saved, 0, board.length);
		return saved;
	}
	
	void initFromString(String string) {
		String[] values = string.split(" +");
		for (int i = 0; i < board.length; i++) {
			board[i] = Integer.parseInt(values[i]); 
		}
	}
	
	String debugPrint() {
		StringBuilder builder = new StringBuilder();
		for (int row = 0; row < dificulty.getBoardSize(); row++) {
			if (row > 0) builder.append('\n');
			
			for (int column = 0; column < dificulty.getBoardSize(); column++) {
				builder.append(String.format("%5d", getCellValue(row, column)));
			}	
		}
		return builder.toString();
	}
	
	public static void main(String[] args) {
//		private void onCellCreated(int index) {
//			// TODO handle animation and undo
//			
//		}
		GameEngine engine = new GameEngine(Dificulty.MEDIUM);
//		engine.initFromString(
//				"0 0 2 0 " +
//				"0 2 0 2 " +
//				"4 2 0 8 "+
//				"4 4 0 8 ");
//		System.out.println(engine.debugPrint());
//		System.out.println();
//		engine.handleUserMove(UserMove.LEFT);
//		System.out.println(engine.debugPrint());
		
		engine.initFromString(
				"2 4 8 16 " +
				"2 2 2 2 " +
				"4 2 2 4 "+
				"4 4 4 4 ");
		System.out.println(engine.debugPrint());
		System.out.println();
		engine.handleUserMove(UserMove.DOWN);
		System.out.println(engine.debugPrint());
	}
}
